import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from '../components/home/';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import ProfileScreen from '../components/profile/';
import {faUser, faHouseDamage} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const MainStack = createBottomTabNavigator(
  {
    homeScreen: {screen: HomeScreen},
    profileScreen: {screen: ProfileScreen},
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'homeScreen') {
          iconName = faHouseDamage;
        } else if (routeName === 'profileScreen') {
          iconName = faUser;
        }

        // You can return any component that you like here!
        return <FontAwesomeIcon icon={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  },
);

const AppNavigator = createSwitchNavigator(
  {
    mainStack: {screen: MainStack},
  },
  {
    headerMode: 'float',
    initialRouteName: 'mainStack',
  },
);

export default createAppContainer(AppNavigator);
