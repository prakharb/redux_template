import {combineReducers} from 'redux';
import homeReducer from './home';
import profileReducer from './profile';

/**
 * The combineReducers helper function turns an object whose values are different
 * reducing functions into a single reducing function you can pass to createStore
 */
const BaseReducer = combineReducers({
  homeReducer,
  profileReducer,
});

const AppReducer = (state, action) => {
  return BaseReducer(state, action);
};

export default AppReducer;
