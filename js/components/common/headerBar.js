import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
const width = Dimensions.get('screen').width;

const HeaderBar = ({left, right, title}) => {
  return (
    <View style={style.container}>
      {left ? left() : null}
      {title ? <Text>{title}</Text> : null}
      {right ? right() : null}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
    width: width,
    borderBottomColor: 'gray',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

export default HeaderBar;
