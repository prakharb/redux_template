import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import HeaderBar from '../common/headerBar';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUserCircle, faSmile} from '@fortawesome/free-regular-svg-icons';

const headerLeft = () => {
  return <FontAwesomeIcon icon={faUserCircle} size={45} />;
};

const headerRight = () => {
  return <FontAwesomeIcon icon={faSmile} size={20} />;
};

const HomeScreen = () => {
  return (
    <>
      <HeaderBar title={'Home Screen'} left={headerLeft} right={headerRight} />
      <View style={style.container}>
        <Text>{'HomeScreen'}</Text>
      </View>
    </>
  );
};

export default HomeScreen;

const style = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
});
