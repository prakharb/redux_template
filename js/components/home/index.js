import HomeScreen from './Screen';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {};
};

export default withNavigation(connect(mapStateToProps)(HomeScreen));
