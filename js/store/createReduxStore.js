import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import AppReducer from '../reducers';

const logger = store => next => action => {
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
};

const createReduxStore = () => {
  /**
   * common use case for middleware is to support asynchronous actions
   * redux-thunk lets the action creators invert control by dispatching functions.
   * They would receive dispatch as an argument and may call it asynchronously.
   * More about middleware at https://redux.js.org/api/applymiddleware
   */
  const enhancer = __DEV__
    ? compose(applyMiddleware(thunk, logger))
    : compose(applyMiddleware(thunk));
  // Store holds the complete state tree of the app
  let store = createStore(AppReducer, enhancer);
  return store;
};

export default createReduxStore;
