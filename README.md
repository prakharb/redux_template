# redux template

Base code for building react-native projects. 

React Native version > 0.60

Redux version > 4.0

Clone the project and run "npm install" from terminal/command-prompt 

Includes fontawesome, react-navigation, react-navigation-tabs and svg libraries.

Run pod init from ios folder and install pods.

For DB you can use realm for react-native.