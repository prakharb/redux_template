/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import createReduxStore from './js/store/createReduxStore';
import AppNavigator from './js/navigators/AppNavigator';

const store = createReduxStore();

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.safeAreaView}>
        {/* <Provider /> is the higher-order component provided
        by React Redux that lets you bind Redux to Reac */}
        <Provider store={store}>
          {/* We will wrap <AppNaivgator /> in <Provider /> so that route
          handlers can get access to the store */}
          <AppNavigator />
        </Provider>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
});

export default App;
